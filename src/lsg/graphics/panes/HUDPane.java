package lsg.graphics.panes;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.transform.Scale;
import lsg.graphics.widgets.characters.statbars.StatBar;
import lsg.graphics.widgets.skills.SkillBar;
import lsg.graphics.widgets.texts.GameLabel;

public class HUDPane extends BorderPane {

    private MessagePane messagePane ;
    private StatBar heroStatBar, monserStatBar ;

    private SkillBar skillBar;
    private IntegerProperty score = new SimpleIntegerProperty();
    private GameLabel scoreLabel;

    public SkillBar getSkillBar() {
        return skillBar;
    }

    public IntegerProperty scoreProperty() {
        return score;
    }

    public HUDPane() {
        this.setPadding(new Insets(80, 10, 10, 10));
        buildCenter() ;
        buildTop() ;
        buildBottom();

        score.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                scoreLabel.setText(newValue.toString());
            }
        });
    }

    public MessagePane getMessagePane() {
        return messagePane;
    }

    public StatBar getHeroStatBar() {
        return heroStatBar;
    }

    public StatBar getMonserStatBar() {
        return monserStatBar;
    }

    private void buildTop(){
        BorderPane borderPane = new BorderPane() ;
        this.setTop(borderPane);

        heroStatBar = new StatBar() ;
        borderPane.setLeft(heroStatBar);

        monserStatBar = new StatBar() ;
        borderPane.setRight(monserStatBar);
        monserStatBar.flip();

        scoreLabel = new GameLabel();
        scoreLabel.setText("0");
        Scale scale = new Scale();
        scale.setX(1.3);
        scale.setY(1.3);
        scoreLabel.getTransforms().add(scale);
        scoreLabel.setTranslateY(40);
        borderPane.setCenter(scoreLabel);
    }

    private void buildCenter(){
        messagePane = new MessagePane() ;
        this.setCenter(messagePane);
    }

    private void buildBottom(){
        skillBar = new SkillBar();
        this.setBottom(skillBar);
    }

}
