package lsg.graphics.widgets.skills;

import javafx.animation.ScaleTransition;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import lsg.graphics.CSSFactory;

public class SkillTrigger extends AnchorPane {

    /** Attributes **/
    private ImageView view;
    private Label text;
    private KeyCode keyCode;
    private SkillAction action;
    private ColorAdjust desaturate;

    /** Getters and setters **/
    public Label getText() {
        return text;
    }

    public void setText(Label text) {
        this.text = text;
    }

    public KeyCode getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    public SkillAction getAction() {
        return action;
    }

    public void setAction(SkillAction action) {
        this.action = action;
    }

    public Image getImage(){
        return view.getImage();
    }

    public void setImage(Image image){
        this.view.setImage(image);
    }

    /** Constructor **/
    public SkillTrigger(KeyCode keyCode,String text,Image image,SkillAction action){
        view = new ImageView();
        this.keyCode = keyCode;
        this.action = action;
        setText(new Label(text));

        desaturate = new ColorAdjust();
        desaturate.setBrightness(0.6);
        desaturate.setSaturation(-1);

        buildUI();
        addListeners();
    }

    /** Trigger's style **/
    private void buildUI() {
        this.getStylesheets().add(CSSFactory.getStyleSheet("SkillTrigger.css"));

        this.getStyleClass().add("skill");
        view.setFitHeight(50);
        view.setFitWidth(50);

        AnchorPane.setTopAnchor(view, 0.0);
        AnchorPane.setBottomAnchor(view, 0.0);
        AnchorPane.setLeftAnchor(view, 0.0);
        AnchorPane.setRightAnchor(view, 0.0);
        this.getChildren().add(view);


        text.getStyleClass().add("skill-text");
        text.setAlignment(Pos.CENTER);
        AnchorPane.setTopAnchor(text, 0.0);
        AnchorPane.setBottomAnchor(text, 0.0);
        AnchorPane.setLeftAnchor(text, 0.0);
        AnchorPane.setRightAnchor(text, 0.0);
        this.getChildren().add(text);
    }

    /** Execute action **/
    public void trigger(){
        if(!this.isDisabled() && action != null){
            animate();
            action.execute();
        }
    }

    /** Listener on the trigger **/
    private void addListeners(){
        this.onMouseClickedProperty().addListener(event -> trigger());

        this.disabledProperty().addListener(event -> {
            if(disabledProperty().getValue()) setEffect(desaturate);
            else setEffect(null);
        });
    }

    /** Trigger's animation **/
    private void animate(){
        ScaleTransition st = new ScaleTransition(Duration.millis(100)) ;
        st.setToX(1.3);
        st.setToY(1.3);
        st.setAutoReverse(true);
        st.setNode(this);
        st.setCycleCount(2);
        st.play();
    }
}
