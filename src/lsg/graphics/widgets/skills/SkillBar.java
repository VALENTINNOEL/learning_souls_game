package lsg.graphics.widgets.skills;

import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

import java.util.LinkedHashMap;
import java.util.Map;

public class SkillBar extends HBox {

    /** Attributes **/
    private SkillTrigger triggers[];
    private ConsumableTrigger consumableTrigger = new ConsumableTrigger(KeyCode.C,"c",null,null);

    /** Binding **/
    private static LinkedHashMap<KeyCode,String> DEFAULT_BINDING = new LinkedHashMap<>();
    static{
        DEFAULT_BINDING.put(KeyCode.DIGIT1,"&");
        DEFAULT_BINDING.put(KeyCode.DIGIT2,"é");
        DEFAULT_BINDING.put(KeyCode.DIGIT3,"\"");
        DEFAULT_BINDING.put(KeyCode.DIGIT4,"'");
        DEFAULT_BINDING.put(KeyCode.DIGIT5,"(");
    }

    /** Constructor **/
    public SkillBar(){
        this.setSpacing(10);
        this.prefHeight(110);
        this.setAlignment(Pos.CENTER);
        this.setStyle("-fx-padding: 15px");
        init();
    }

    /** Init the tab of triggers **/
    private void init(){
        triggers = new SkillTrigger[DEFAULT_BINDING.size()];
        int i = 0;

        for (Map.Entry<KeyCode, String > binding : DEFAULT_BINDING.entrySet()){
            triggers[i] = new SkillTrigger(binding.getKey(), binding.getValue(), null, null);
            this.getChildren().add(triggers[i]);
            i++;
        }

        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(30);

        this.getChildren().addAll(rectangle,consumableTrigger);
    }

    /** Guetters **/
    public SkillTrigger getTrigger(int slot){
        return triggers[slot];
    }

    public ConsumableTrigger getConsumableTrigger() {
        return consumableTrigger;
    }

    /** Execute the trigger **/
    public void process(KeyCode code){
        if(!this.isDisabled()){
            for(SkillTrigger trigger : triggers){
                if(trigger.getKeyCode() == code){
                    trigger.trigger();
                    return;
                }
            }
            if(consumableTrigger.getKeyCode() == code){
                consumableTrigger.trigger();
                return;
            }
        }
    }
}
