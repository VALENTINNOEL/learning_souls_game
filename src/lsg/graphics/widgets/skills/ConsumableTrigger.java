package lsg.graphics.widgets.skills;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import lsg.consumables.Consumable;
import lsg.graphics.CSSFactory;
import lsg.graphics.CollectibleFactory;

public class ConsumableTrigger extends SkillTrigger{

    /** Attributes **/
    private Consumable consumable;

    /** Setter **/
    public void setConsumable(Consumable consumable){

        if(consumable == null) return;

        this.consumable = consumable;

        this.consumable.isEmptyProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                setDisable(newValue);
            }
        });

        this.setImage(CollectibleFactory.getImageFor(consumable));
    }

    /**
     * Constructor
     *
     * @param keyCode
     * @param text
     * @param consumable
     * @param action
     **/
    public ConsumableTrigger(KeyCode keyCode, String text,Consumable consumable, SkillAction action) {

        super(keyCode,text,null,action);

        this.getStylesheets().add(CSSFactory.getStyleSheet("ConsumableTrigger.css"));
        this.getStyleClass().add("consumable");

        setConsumable(consumable);


    }
}
